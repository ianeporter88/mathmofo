program linear_interpolation_test
    use Precision
    use Linear_interpolation, only : linterp_1d
    use mathmofoPassFail,     only : analyze, all_tests_pass
    implicit none
    !! author: Ian Porter
    !! date: 2/10/2019
    !!
    !! This tests the linear interpolation routines
    !!
    real(r8k), parameter :: known     = 4.0_r8k    !! Known value
    real(r8k), parameter :: tolerance = 1.0e-8_r8k !! Tolerance limit
    real(r8k), parameter :: val       = 5.0_r8k    !! X value
    real(r8k) :: calc         !! Calculated value
    logical   :: test_passed  !! Flag for determining passing test
    type(linterp_1d) :: example_1d

    call example_1d%setup(x=[0.0_r8k, 10.0_r8k], y=[2.0_r8k, 6.0_r8k], bound=.false.)

    calc = example_1d%get(val)

    test_passed = Analyze(calc, known, tolerance)

    if (test_passed) call all_tests_pass ()

end program linear_interpolation_test
