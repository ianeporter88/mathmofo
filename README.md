# mathmofo

This library contains basic math functions written in modern Fortran.

The current functionality includes:
- [ ] Linear interpolation
  - [ ] 1D

Feel free to contribute!
