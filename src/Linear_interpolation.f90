module linear_interpolation
    use precision
    implicit none
    !! author: Ian Porter
    !! date: 2/10/2019
    !!
    !! This module contains linear interpolation routines
    !!
    private
    public :: linterp_1d

    type, abstract :: linterp
        !! Abstract linear interpolation class
        logical :: bound = .false.   !! Flag to indicate whether to bound extrapolation
    end type linterp

    type, extends(linterp) :: linterp_1d
        !! 1D linear interpolation
        real(r8k), dimension(:), allocatable :: x  !! array
        real(r8k), dimension(:), allocatable :: y  !! array
    contains
        procedure :: setup => linterp_1d_setup
        procedure :: get   => linterp_1d_get
    end type linterp_1d

    interface

        module subroutine linterp_1d_setup (me, x, y, bound)
        !! author: Ian Porter
        !! date: 2/10/2019
        !!
        !! This routine sets up the DT of linterp_1d
        !!
        class(linterp_1d),       intent(out) :: me    !! DT
        real(r8k), dimension(:), intent(in)  :: x     !! Array of x values
        real(r8k), dimension(:), intent(in)  :: y     !! Array of y values
        logical, optional,       intent(in)  :: bound !! Flag to indicate whether to bound array

        end subroutine linterp_1d_setup

        module function linterp_1d_get (me, x) result (val)
        !! author: Ian Porter
        !! date: 2/10/2019
        !!
        !! This routine implements the solution to solving the 1d linear interpolation routine
        !!
        class(linterp_1d), intent(in) :: me  !! DT
        real(r8k),         intent(in) :: x   !! Interpolation value
        real(r8k)                     :: val !! Result

        end function linterp_1d_get

    end interface

end module linear_interpolation
