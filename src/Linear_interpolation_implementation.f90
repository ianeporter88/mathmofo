submodule (linear_interpolation) Linear_interpolation_implementation
    implicit none
    !! author: Ian Porter
    !! date: 2/10/2019
    !!
    !! This submodule implements the solution to solving the linear interpolation routines
    !!
    contains

        module procedure linterp_1d_setup
        !! author: Ian Porter
        !! date: 2/10/2019
        !!
        !! This routine sets up the DT of linterp_1d
        !!
        integer(i4k) :: i

        if (size(me%x) /= size(me%y)) then
            error stop 'Execution terminated in linterp_1d_setup. Size of x /= size of y.'
        else if (size(me%x) == 0) then
            error stop 'Execution terminated in linterp_1d_setup. Size of x = 0.'
        end if

        if (allocated(me%x)) deallocate(me%x)
        if (allocated(me%y)) deallocate(me%y)

        me%x = x
        me%y = y

        do i = 1, size(me%x) - 1
            if (me%x(i+1) <= me%x(i)) then
                error stop 'Execution terminated in linterp_1d_setup. Array x is not in increasing order.'
            end if
        end do

        end procedure linterp_1d_setup

        module procedure linterp_1d_get
        !! author: Ian Porter
        !! date: 2/10/2019
        !!
        !! This routine implements the solution to solving the 1d linear interpolation routine
        !!
        integer, dimension(2) :: position

        call get_bounds(me%x,x,me%bound,position)

        val = linearly_interpolate (x, me%x(position(1)), me%x(position(2)), &
          &                         me%y(position(1)), me%y(position(2)))

        end procedure linterp_1d_get

        subroutine get_bounds (array, x, bound, position)
        real(r8k),    dimension(:), intent(in)  :: array     !! Array to interpolate on
        real(r8k),                  intent(in)  :: x         !! Interpolation value
        logical,                    intent(in)  :: bound     !! Flag for whether to bound array
        integer(i4k), dimension(2), intent(out) :: position  !! Index bounds
        integer(i4k) :: i

        associate (array_len => size(array))   !! Length of array
        position = 0

        if (array_len == 1) then
            !! If only 1 value provided, then assume result is that one value
            position(1) = 1; position(2) = 1
            return
        end if

        index_search: do i = 1, array_len
            if (x <= array(i)) then
                position(1) = i
            else
                exit index_search
            end if
        end do index_search

        if      (position(1) == 0) then
            if (bound) then
                position(1) = 1
            else
                position(1) = 1; position(2) = 2
            end if
        else if (position(1) == array_len) then
            if (bound) then
                position(2) = array_len
            else
                position(2) = array_len; position(1) = array_len - 1
            end if
        else
            position(2) = position(1) + 1
        end if

        end associate

        end subroutine get_bounds

        impure function linearly_interpolate (val, x1, x2, y1, y2) result (interp)
        !! author: Ian Porter
        !! date: 2/10/2019
        !!
        !! Linear interpolation function
        !!
        real(r8k) :: val     !! Interpolation x value
        real(r8k) :: x1      !! Lower bound x value
        real(r8k) :: x2      !! Upper bound x value
        real(r8k) :: y1      !! Lower bound y value
        real(r8k) :: y2      !! Upper bound y value
        real(r8k) :: interp  !! Function result

        if (x1 == x2) then
            !! This is a bounded calculation
            if (y1 /= y2) then
                error stop 'Execution terminated in linterp. If x1=x2, y1 must equal y2.'
            end if
            interp = y1
        else
            interp = y1 + (y2 - y1) / (x2 - x1) * (val - x1)
        end if

      end function linearly_interpolate

end submodule Linear_interpolation_implementation
