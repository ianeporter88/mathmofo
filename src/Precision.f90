module Precision
    use iso_fortran_env, only : i4k => int32, i8k => int64, r4k => real32, r8k => real64
    implicit none
    !! author: Ian Porter
    !! date: 2/10/2019
    !!
    !! This module contains the precision used by the math library
    !!
    !! i4k - single precision integer
    !! i8k - double precision integer
    !! r4k - single precision real
    !! r8k - double precision real

end module Precision
